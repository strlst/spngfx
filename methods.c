#include "methods.h"

struct methods t[METHODS] = {   
    { "add",          &add          },
    { "subtract",     &subtract     },
    { "average",      &average      },
    { "flip",         &flip         },
    { "invert",       &invert       },
    { "toon",         &toon         },
    { "burn",         &burn         },
    { "scanlines",    &scanlines    },
    { "pinkify",      &pinkify      },
    { "superpos",     &superpos     },
    { "edge",         &edge         },
    { "gauss",        &gauss        },
    { "none",         NULL          }
};

void add(spng *png, uint8_t count, uint8_t index) {
    // three separate color components
    png_byte c1, c2, c3;
    float applier = 1.f + (0.1f * count);
    uint8_t o1 = (index + 1) % 3, o2 = (index + 2) % 3;

    if (index == 0xFF) {
        add(png, count, 0);
        add(png, count, 1);
        add(png, count, 2);
    } else {
	    index %= 3;

	    for (uint32_t y = 0; y < png->height; y++) {
		png_byte* row = png->row_pointers[y];
		for (uint32_t x = 0; x < png->width; x++) {
		    png_byte *ptr = &(row[x * png->channels]);

		    // get three color components
		    c1 = ptr[index];
		    c2 = ptr[o1];
		    c3 = ptr[o2];

		    ptr[index] = (c1 * applier) > MAX_BRT ? MAX_BRT : (c1 * applier);
		    ptr[o1] = c2;
		    ptr[o2] = c3;
		}
	    }
    }
}

void subtract(spng *png, uint8_t count, uint8_t index) {
    // three separate color components
    png_byte c1, c2, c3;
    float applier = 1.f - (0.1f * count);
    uint8_t o1 = (index + 1) % 3, o2 = (index + 2) % 3;

    index %= 3;

    if (index == 0xFF) {
        subtract(png, count, 0);
        subtract(png, count, 1);
        subtract(png, count, 2);
    } else {
	    for (uint32_t y = 0; y < png->height; y++) {
		png_byte* row = png->row_pointers[y];
		for (uint32_t x = 0; x < png->width; x++) {
		    png_byte *ptr = &(row[x * png->channels]);

		    // get three color components
		    c1 = ptr[index];
		    c2 = ptr[o1];
		    c3 = ptr[o2];

		    ptr[index] = (c1 * applier) < 0 ? 0 : (c1 * applier);
		    ptr[o1] = c2;
		    ptr[o2] = c3;
		}
	    }
    }
}

void average(spng *png, uint8_t count, uint8_t index) {
    // three separate color components
    png_byte c2, c3;
    uint8_t o1 = (index + 1) % 3, o2 = (index + 2) % 3;

    index %= 3;

    for (uint32_t y = 0; y < png->height; y++) {
        png_byte* row = png->row_pointers[y];
        for (uint32_t x = 0; x < png->width; x++) {
            png_byte *ptr = &(row[x * png->channels]);

            // get three color components
            c2 = ptr[o1];
            c3 = ptr[o2];

            ptr[index] = (c2 + c3) / (count * 2.f);
        }
    }
}

void flip(spng *png, uint8_t count, uint8_t param) {
    /* flipping twice does nothing */
    if (count % 2 == 0)
        return;

    spng *copy = copypng(png);

    for (uint32_t y = 0; y < png->height; y++) {
        png_byte* row = png->row_pointers[y];
        png_byte* opp_row = copy->row_pointers[(png->height - 1) - y];
        if (y == png->height - y)
            continue;
        for (uint32_t x = 0; x < png->width; x++) {
            png_byte *ptr = &(row[x * png->channels]);
            png_byte *ptr2 = &(opp_row[x * png->channels]);
            ptr[0] = ptr2[0];
            ptr[1] = ptr2[1];
            ptr[2] = ptr2[2];
        }
    }

    freepng(copy);
}

void invert(spng *png, uint8_t count, uint8_t param) {
    /* flipping twice does nothing */
    if (count % 2 == 0)
        return;

    png_byte r, g, b;
    for (uint32_t y = 0; y < png->height; y++) {
        png_byte* row = png->row_pointers[y];
        for (uint32_t x = 0; x < png->width; x++) {
            png_byte *ptr = &(row[x * png->channels]);
            r = ptr[0];
            g = ptr[1];
            b = ptr[2];
            ptr[0] = 255 - r;
            ptr[1] = 255 - g;
            ptr[2] = 255 - b;
        }
    }
} 

void toon(spng *png, uint8_t count, uint8_t step) {
    png_byte r, g, b;

    /* use reasonable default value */
    if (step == 0xFF)
        step = 16;

    for (uint32_t y = 0; y < png->height; y++) {
        png_byte* row = png->row_pointers[y];
        for (uint32_t x = 0; x < png->width; x++) {
            png_byte *ptr = &(row[x * png->channels]);

            r = ptr[0];
            g = ptr[1];
            b = ptr[2];

            r -= count * (r % step);
            g -= count * (g % step);
            b -= count * (b % step);

            ptr[0] = r;
            ptr[1] = g;
            ptr[2] = b;
        }
    }
}

void burn(spng *png, uint8_t count, uint8_t param) {
    double threshhold = param == 0xFF ? .66f : ((double) param) / ((double) 0xFF);
    double reducer = .5f;
    uint8_t biggest;
    png_byte r, g, b;

    for (uint32_t y = 0; y < png->height; y++) {
        png_byte* row = png->row_pointers[y];
        for (uint32_t x = 0; x < png->width; x++) {
            png_byte *ptr = &(row[x * png->channels]);

            r = ptr[0];
            g = ptr[1];
            b = ptr[2];

            biggest = r > g ? (r > b ? r : b) : (g > b ? g : b);
            if (r / 255.f < threshhold && r != biggest)
                r *= (reducer * (float) count);
            if (g / 255.f < threshhold && g != biggest)
                g *= (reducer * (float) count);
            if (b / 255.f < threshhold && b != biggest)
                b *= (reducer * (float) count);

            ptr[0] = r;
            ptr[1] = g;
            ptr[2] = b;
        }
    }
}

// TODO: static effect method

void scanlines(spng *png, uint8_t count, uint8_t thickness) {
    /* for specific color accentuation */
    double accent_r = 1.f - (count * .099f);
    double accent_g = 1.f;
    double accent_b = 1.f - (count * .099f);
    /* effect strength */
    double fx;
    /* scanline size */
    uint8_t size = thickness == 0xFF ? 6 : thickness;
    png_byte r, g, b;
    for (uint32_t y = 0; y < png->height; y++) {
        /* scanline relative pixel pos */
        uint32_t pos = y % size;
        /* booleans */
        if (pos >= (size / 2.5f))
            fx = .8;
        else
            fx = 1.;

        png_byte* row = png->row_pointers[y];
        for (uint32_t x = 0; x < png->width; x++) {
            png_byte *ptr = &(row[x * png->channels]);

            r = ptr[0] * (fx * accent_r);
            g = ptr[1] * (fx * accent_g);
            b = ptr[2] * (fx * accent_b);

            ptr[0] = r;
            ptr[1] = g;
            ptr[2] = b;
        }
    }
}

void pinkify(spng *png, uint8_t count, uint8_t param) {
    png_byte r, g, b;

    double r_to_g     = 0.;
    double b_to_g     = 0.;
    double mean_ratio = 0.;

    /* crazy people might apply this method multiple times */
    for (uint8_t c = 0; c < count; c++) {
        for (uint32_t y = 0; y < png->height; y++) {
            png_byte* row = png->row_pointers[y];
            for (uint32_t x = 0; x < png->width; x++) {
                png_byte *ptr = &(row[x * png->channels]);

                r = ptr[0];
                g = ptr[1];
                b = ptr[2];

                r_to_g = g / (r + 0.000001f);
                b_to_g = g / (b + 0.000001f);
                mean_ratio = (r_to_g + b_to_g) / 2;

                if (mean_ratio > 1)
                    g *= .1f;
                else
                    g *= mean_ratio;
                b *= .9f;
                r *= 1.4f;

                ptr[0] = r > 255 ? 255 : r;
                ptr[1] = g < 0 ? 0 : g;
                ptr[2] = b;
            }
        }
    }
}

void superpos(spng *png, uint8_t count, uint8_t param) {
    png_byte r, g, b;
    /* 235,180,93 for nixie accent */
    uint8_t accent_r = 255;
    uint8_t accent_g = 128;
    uint8_t accent_b = 255;

    /* crazy people might apply this method multiple times */
    for (uint8_t c = 0; c < count; c++) {
        for (uint32_t y = 0; y < png->height; y++) {
            png_byte* row = png->row_pointers[y];
            for (uint32_t x = 0; x < png->width; x++) {
                png_byte *ptr = &(row[x * png->channels]);
                r = ptr[0];
                g = ptr[1];
                b = ptr[2];
                ptr[0] = (b + ((x * param) % param) + (y % (param / 2))) % accent_r;
                ptr[1] = (r + ((x * param) % param) + (y % (param / 2))) % accent_g;
                ptr[2] = (g + ((x * param) % param) + (y % (param / 2))) % accent_b;
            }
        }
    }
}

void edge(spng *png, uint8_t count, uint8_t param) {
    double kernel[9] = {
        -1.0, -1.0, -1.0,
        -1.0,  8.0, -1.0,
        -1.0, -1.0, -1.0
    };

    for (uint8_t c = 0; c < count; c++)
        apply_kernel(png, &kernel, 9);
}

void gauss(spng *png, uint8_t count, uint8_t param) {
    double kernel[9] = {
        .0625, .125, .0625,
        .125,   4.0, .125,
        .0625, .125, .0625
    };

    for (uint8_t c = 0; c < count; c++)
        apply_kernel(png, &kernel, 9);
}

void apply_method(spng *png, uint8_t count, uint8_t param, void(*method)(spng*, uint8_t, uint8_t)) {
    (*method)(png, count, param);
}

void apply_kernel(spng *png, double (*kernel)[], uint32_t kernel_size) {
    spng *copy = copypng(png);

    /* TODO: make all this shit generic lol */
    for (uint32_t y = 0; y < png->height; y++) {
        uint32_t prev_y = y > 0               ? (y - 1) : 0;
        uint32_t next_y = y < png->height - 1 ? (y + 1) : y;
        /* acquire 3 by 3 matrix of pixels */
        png_byte* row[3] = {
            copy->row_pointers[prev_y],
            copy->row_pointers[y],
            copy->row_pointers[next_y]
        };
        /* acquire image row of target buffer */
        png_byte* orig_row = png->row_pointers[y];

        for (uint32_t x = 1; x < png->width; x++) {

            uint32_t prev_x = x > 0              ? (x - 1) : 0;
            uint32_t next_x = x < png->width - 1 ? (x + 1) : x;
            png_byte *ptr[9] = {
                &(row[0][prev_x * png->channels]),
                &(row[0][x      * png->channels]),
                &(row[0][next_x * png->channels]),
                &(row[1][prev_x * png->channels]),
                &(row[1][x      * png->channels]),
                &(row[1][next_x * png->channels]),
                &(row[2][prev_x * png->channels]),
                &(row[2][x      * png->channels]),
                &(row[2][next_x * png->channels])
            };
            /* acquire pixel of target buffer */
            png_byte *orig_ptr = &(orig_row[x * png->channels]);

            /* 0  1  2 */
            /* 3  4  5 */
            /* 6  7  8 */
            /* where 4 represents the local pixel */
            png_byte r[9] = {
                ptr[0][0], ptr[1][0], ptr[2][0],
                ptr[3][0], ptr[4][0], ptr[5][0],
                ptr[6][0], ptr[7][0], ptr[8][0]
            };
            png_byte g[9] = {
                ptr[0][1], ptr[1][1], ptr[2][1],
                ptr[3][1], ptr[4][1], ptr[5][1],
                ptr[6][1], ptr[7][1], ptr[8][1]
            };
            png_byte b[9] = {
                ptr[0][2], ptr[1][2], ptr[2][2],
                ptr[3][2], ptr[4][2], ptr[5][2],
                ptr[6][2], ptr[7][2], ptr[8][2]
            };

            png_byte brt[9];
            /* luminance conversion */
            /* digital ITU BT.601 */
            /* 0.299 R + 0.587 G + 0.114 B */
            for (uint8_t i = 0; i < 9; i++)
                brt[i] =
                    .299 * r[i] +
                    .587 * g[i] +
                    .114 * b[i];

            /* TODO: */
            /* obv not every kernel operation assumes luminance input */
            /* values, needs changing to */
            uint8_t lum = mult_by_kernel(&brt, kernel, kernel_size);

            orig_ptr[0] = lum;
            orig_ptr[1] = lum;
            orig_ptr[2] = lum;
        }
    }

    freepng(copy);
}

uint8_t mult_by_kernel(png_byte (*src)[], double (*kernel)[], uint32_t kernel_size) {
    double sum = 0;
    for (uint32_t i = 0; i < kernel_size; i++)
        sum += (*src)[i] * (*kernel)[(kernel_size - 1) - i];
    sum = sum < 0. ? 0. : sum;
    return (uint8_t)sum;
}
