CFLAGS:= -Wall -std=c99 -pedantic
CXX:= gcc
PROG:= spngfx

LDFLAGS:=
LDFLAGS+= -l spngstream

OBJS:= methods.o main.o

all: $(PROG)

%.o: %.c
	$(CXX) $(CFLAGS) -c $<

$(PROG): $(OBJS)
	$(CXX) $(CFLAGS) -g $(OBJS) -o $(PROG) $(LDFLAGS)
	rm *.o

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${PROG} ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${PROG}

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/${PROG}

clean:
	rm -f $(PROG) *.o