# spngfx

smol example implementation of libspngstream functionality for tinker and future reference purposes

```
usage: spngfx [-vl] [-f ifile] [-o ofile] [-m method]
       -f:     reads from stdin if not specified
       -o:     outputs to stdout if not specified
       -l:     list methods
       -h:     prints this usage
       -v:     print version
```
