#ifndef METHODS_H
#define METHODS_H

#include <stdint.h>

#include "libspngstream.h"

#define METHODS 13
#define MAX_APPLICATIONS 0xFF
#define MAX_BRT 254

/* methods */
void add(spng*, uint8_t, uint8_t);
void subtract(spng*, uint8_t, uint8_t);
void average(spng*, uint8_t, uint8_t);
void flip(spng*, uint8_t, uint8_t);
void invert(spng*, uint8_t, uint8_t);
void toon(spng*, uint8_t, uint8_t);
void burn(spng*, uint8_t, uint8_t);
void scanlines(spng*, uint8_t, uint8_t);
void pinkify(spng*, uint8_t, uint8_t);
void superpos(spng*, uint8_t, uint8_t);
void edge(spng*, uint8_t, uint8_t);
void gauss(spng*, uint8_t, uint8_t);

struct methods {
    char *name;
    void (*f)(spng*, uint8_t, uint8_t);
};

extern struct methods t[METHODS];

/* utility functions */
void apply_method(spng*, uint8_t, uint8_t, void(*method)(spng*, uint8_t, uint8_t));
void apply_kernel(spng*, double(*k)[], uint32_t);
uint8_t mult_by_kernel(png_byte(*s)[], double(*k)[], uint32_t);

#endif
